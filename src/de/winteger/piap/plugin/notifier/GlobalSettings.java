package de.winteger.piap.plugin.notifier;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;

public class GlobalSettings {
	private static final String TAG = "LoggerNotification";
	
	private static final String CONTACTS = "contacts";
	private static final String PREF_ID = "MyPreferences";

	public static String getContacts(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREF_ID, 0);
		String savedContacts = settings.getString(CONTACTS, "");
		return savedContacts;
	}

	public static void setContacts(Context context, String contacts) {
		SharedPreferences settings = context.getSharedPreferences(PREF_ID, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(CONTACTS, contacts);
		editor.commit();
	}

	public static String[] getContactsArray(Context context) {
		String numbersConcat = getContacts(context);
		if (numbersConcat.length() <= 1) {
			return null;
		}
		String[] numbers = numbersConcat.split(";");
		return numbers;
	}

	/**
	 * Returns a match of contact name and contact id from the contacts as
	 * String array
	 * 
	 * @param context
	 *            The context
	 * @param phoneNumber
	 *            The phoneNumber
	 * @return The match
	 */
	public static String[] getContact(Context context, String phoneNumber) {
		Log.i(TAG, "getContact(" + phoneNumber + ")");
		String contactId = "-1";
		String contactName = "";
		ContentResolver localContentResolver = context.getContentResolver();
		Cursor contactLookupCursor = localContentResolver.query(Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber)), new String[] {
				PhoneLookup.DISPLAY_NAME, PhoneLookup._ID }, null, null, null);
		try {
			while (contactLookupCursor.moveToNext()) {
				contactName = contactLookupCursor.getString(contactLookupCursor.getColumnIndex(PhoneLookup.DISPLAY_NAME));
				contactId = contactLookupCursor.getString(contactLookupCursor.getColumnIndex(PhoneLookup._ID));
				Log.d(TAG, "contactMatch name: " + contactName);
				Log.d(TAG, "contactMatch id: " + contactId);

			}
		} finally {
			contactLookupCursor.close();
		}
		return new String[] { contactName, contactId };
	}
}