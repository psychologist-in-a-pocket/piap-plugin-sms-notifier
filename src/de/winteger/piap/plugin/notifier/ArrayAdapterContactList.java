package de.winteger.piap.plugin.notifier;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import de.winteger.piap.plugin.notifier.R;

public class ArrayAdapterContactList extends ArrayAdapter<ContactListItem> {

	private final String TAG = "LoggerNotification";

	private LayoutInflater inflater; // Obtains the layout
	private ArrayList<ContactListItem> entryList; // List of contacts that is displayed in a ListView
	private static ArrayAdapterContactList me = null;

	/**
	 * Creates new ArrayAdapterInputLogs with the given parameters
	 * 
	 * @param context
	 *            The context
	 * @param inflater
	 *            The inflater
	 * @param textViewResourceId
	 *            The textViewResourceId
	 * @param objects
	 *            The list of contacts
	 */
	public ArrayAdapterContactList(Context context, LayoutInflater inflater, int textViewResourceId, List<ContactListItem> objects) {
		super(context, textViewResourceId, objects);
		this.inflater = inflater;
		this.entryList = (ArrayList<ContactListItem>) objects;
		ArrayAdapterContactList.me = this;
	}

	/**
	 * sets the entryList to the list list
	 * 
	 * @param list
	 *            The list
	 */
	public void updateEntryList(ArrayList<ContactListItem> list) {
		entryList = list;
		Log.i(TAG, "List size: " + list.size());
	}

	/**
	 * defines how an item of the list view looks like
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//		if (position == entryList.size() - 1) {
		//			RelativeLayout view = new RelativeLayout(context);
		//			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.FILL_PARENT);
		//			lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		//			TextView tv = new TextView(context);
		//			tv.setTypeface(null, Typeface.ITALIC);
		//			tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
		//			tv.setText("add contact...");
		//			view.addView(tv, lp);
		//
		//			return view;
		//
		//		}

		// get the layout for a list item
		View entry = inflater.inflate(R.layout.lv_default_item, parent, false);

		TextView tvName = (TextView) entry.findViewById(R.id.tvName);
		TextView tvNumber = (TextView) entry.findViewById(R.id.tvNumber);
		ImageView ivImage = (ImageView) entry.findViewById(R.id.ivImage);
		ImageView ivDelete = (ImageView) entry.findViewById(R.id.ivDelete);

		final int pos = position;

		ivDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				entryList.remove(pos);
				me.notifyDataSetChanged();
			}
		});

		// get the input log
		ContactListItem item = entryList.get(position);

		// get data from the contact
		if (item.photo != null) {
			ivImage.setImageDrawable(item.photo);
		}

		// fill the layout with the data
		tvName.setText(item.name);
		tvNumber.setText(item.number);

		return entry;
	}
}
