package de.winteger.piap.plugin.notifier;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

/**
 * This receiver listens for actions to send SMS, react to sent and delivered
 * SMS and to start the contact picker GUI.
 * 
 * @author sarah
 * 
 */
public class NotificationServiceStarter extends BroadcastReceiver {

	private static final String TAG = "LoggerNotification";

	// Fields for the actions in intents
	public static final String SEND = "de.winteger.piap.plugin.notifier.SEND_SMS";
	public static final String SENT = "de.winteger.piap.plugin.notifier.SMS_SENT";
	public static final String DELIVERED = "de.winteger.piap.plugin.notifier.SMS_DELIVERED";
	public static final String MANAGE_CONTACTS = "de.winteger.piap.plugin.notifier.MANAGE_CONTACTS";

	// Fields for the ContentResolver to add the SMS to sent SMS
	private static final String TELEPHON_NUMBER_FIELD_NAME = "address";
	private static final String MESSAGE_BODY_FIELD_NAME = "body";
	private static final Uri SENT_MSGS_CONTENT_PROVIDER = Uri.parse("content://sms/sent");

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "received intent");

		//de.uvwxy.melogsta.Log.setContext(context);
		//de.uvwxy.melogsta.Log.i("SMSN", "Received intent");

		if (intent.getAction().equals(SEND)) {
			// notify contacts via SMS
			startSMSService(context, intent);
		} else if (intent.getAction().equals(SENT)) {
			// SMS was sent
			handleSentSMS(context, intent);
		} else if (intent.getAction().equals(DELIVERED)) {
			// SMS was delivered
			//Log.i(TAG, "SMS delivered intent returned");
			// TODO: what to do here?
		} else if (intent.getAction().equals(MANAGE_CONTACTS)) {
			// Start GUI to pick out contacts for notification
			Intent intentContacts = new Intent(context, ContactsActivity.class);
			intentContacts.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intentContacts.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
			context.startActivity(intentContacts);
		} else {
			// something bad happened...
			//Log.i(TAG, "unkown intent received, this should not happen");
		}
	}

	/**
	 * The intent of the pending intent from sending SMS returned. If sending
	 * was successful the SMS is added to the users SMS outbox. Shows a
	 * notification with the result.
	 * 
	 * @param context
	 *            The context
	 * @param intent
	 *            The intent
	 */
	private void handleSentSMS(Context context, Intent intent) {
		// returned results from the notification
		//Log.i(TAG, "SMS sent intent returned");
		int resultCode = this.getResultCode();
		//Log.i(TAG, "res code " + resultCode);

		// get data out
		Bundle b = intent.getExtras();

		String message = "";
		String phoneNumber = "";
		String contactName = "Unknown";
		int contactId = -1;

		// if data is present
		if (b != null) {
			// read out msg and number
			message = b.getString("message");
			phoneNumber = b.getString("number");
			//Log.i(TAG, "bundle data: " + phoneNumber + " " + message);

			// Find out which contact was notified
			String result[] = GlobalSettings.getContact(context, phoneNumber);

			contactName = result[0];
			contactId = Integer.parseInt(result[1]);

			if (contactId == -1) {
				// no contact found
				contactName = phoneNumber;
			}
		}

		// messages for the notification
		String textSuccess = contactName + context.getString(R.string.has_received_a_sms_about_your_last_evaluation_status);
		String textFail = contactName + context.getString(R.string.could_not_be_notified_via_sms_about_your_last_evaluation_status);

		switch (resultCode) {
		case Activity.RESULT_OK:
			// everything went OK
			//Log.i(TAG, "result ok");

			// add SMS to sent SMS
			addMessageToSent(context, phoneNumber, message);
			// show a notification				
			showNotification(context, context.getString(R.string.notification_sent), textSuccess, contactId);
			break;
		case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
			//Log.i(TAG, "RESULT_ERROR_GENERIC_FAILURE");
			// show a notification
			showNotification(context, context.getString(R.string.notification_failed), textFail, contactId);
			break;
		case SmsManager.RESULT_ERROR_RADIO_OFF:
			//Log.i(TAG, "RESULT_ERROR_RADIO_OFF");
			// show a notification
			showNotification(context, context.getString(R.string.notification_failed), textFail, contactId);
			break;
		case SmsManager.RESULT_ERROR_NULL_PDU:
			//Log.i(TAG, "RESULT_ERROR_NULL_PDU");
			// show a notification
			showNotification(context, context.getString(R.string.notification_failed), textFail, contactId);
			break;
		default:
			//Log.i(TAG, "unknown result");
			// show a notification
			showNotification(context, context.getString(R.string.notification_failed), textFail, contactId);
			break;
		}
	}

	/**
	 * This method starts the SMS Service that sends a SMS to the given number
	 * in the intent data with the given message from the intent data
	 * 
	 * @param context
	 *            The context
	 * @param intent
	 *            The intent
	 */
	private void startSMSService(Context context, Intent intent) {
		//Log.i(TAG, "passing data to SMS service");
		// read out intent data
		Bundle b = intent.getExtras();

		String[] numbers = GlobalSettings.getContactsArray(context);
		String message = b.getString("message");

		//Log.i(TAG, "Numbers found " + numbers.length);
		if (numbers != null) {
			for (int i = 0; i < numbers.length; i++) {
				Bundle data = new Bundle();
				data.putString("number", numbers[i]);
				data.putString("message", message);
				// create SMS intent
				Intent smsIntent = new Intent(context, SMSService.class);
				// pass data to the new sms intent
				smsIntent.putExtra("data", data);
				// start SMS service via intent
				context.startService(smsIntent);
			}
		}
	}

	/**
	 * Shows a new notification if no notification with id id exists, else the
	 * existing notification with id id is updated. Click on the notification
	 * shows the evaluation GUI.
	 * 
	 * @param ctx
	 *            The context
	 * @param title
	 *            The notification title
	 * @param msg
	 *            The notification message
	 * @param id
	 *            The notification id
	 */
	@SuppressLint("InlinedApi")
	private void showNotification(Context ctx, String title, String msg, int id) {
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx);
		mBuilder.setSmallIcon(R.drawable.ic_launcher);
		mBuilder.setContentTitle(title);
		mBuilder.setContentText(msg);

		// Show the evaluation GUI after clicking on the notification
		Intent resultIntent = new Intent("de.winteger.piap.SHOWEVAL");
		resultIntent.addCategory("android.intent.category.DEFAULT");

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1) {
			resultIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
		}

		PendingIntent resultPendingIntent = PendingIntent.getActivity(ctx, id, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification noti = mBuilder.build();

		// Hide the notification after it was selected
		noti.flags |= Notification.FLAG_AUTO_CANCEL;

		nm.notify(id, noti);
	}

	/**
	 * This method adds a SMS to the given number with the given text to the
	 * sent SMS, the SMS appears in the SMS app
	 * 
	 * @param phoneNumber
	 *            The telephone number
	 * @param message
	 *            The text
	 * @param ctx
	 *            The context
	 */
	private void addMessageToSent(Context ctx, String phoneNumber, String message) {
		ContentValues sentSms = new ContentValues();
		sentSms.put(TELEPHON_NUMBER_FIELD_NAME, phoneNumber);
		sentSms.put(MESSAGE_BODY_FIELD_NAME, message);

		ContentResolver contentResolver = ctx.getContentResolver();
		contentResolver.insert(SENT_MSGS_CONTENT_PROVIDER, sentSms);
	}
}