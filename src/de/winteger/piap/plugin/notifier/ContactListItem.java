package de.winteger.piap.plugin.notifier;

import android.graphics.drawable.Drawable;

public class ContactListItem {
	String name = "test";
	String number = "123";
	Drawable photo = null;
	
	public ContactListItem(){
		
	}
	
	/**
	 * 
	 * @param name
	 * @param number
	 */
	public ContactListItem(String name, String number) {
		this.name = name;
		this.number = number;
	}
	
	public void setPhoto(Drawable photo) {
		this.photo = photo;
	}

}
