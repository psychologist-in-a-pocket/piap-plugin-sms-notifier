package de.winteger.piap.plugin.notifier;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import de.winteger.piap.plugin.notifier.R;

public class ContactsActivity extends SherlockActivity {

	private final static String TAG = "LoggerNotification";

	// Result code
	private static final int PICK_CONTACT = 456;

	private ArrayList<ContactListItem> contactList;
	private ArrayAdapterContactList contactArrayAdapter;
	private ListView contactListView;
	private ArrayList<String> phoneNumbers;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts);

		// load numbers from saved settings
		phoneNumbers = new ArrayList<String>();
		String[] numbers = GlobalSettings.getContacts(this).split(";");
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i].length() > 0) {
				phoneNumbers.add(numbers[i]);
			}
		}

		// set up list view
		contactListView = (ListView) findViewById(R.id.lvContacts);
		contactListView.setSelector(android.R.color.transparent);
		contactList = new ArrayList<ContactListItem>();

		contactArrayAdapter = new ArrayAdapterContactList(this, getLayoutInflater(), R.id.lvContacts, contactList);

		fillListWithContacts(contactList);

		contactListView.setAdapter(contactArrayAdapter);
		contactArrayAdapter.notifyDataSetChanged();

		// remove icon + label from action bar
		ActionBar ab = getSupportActionBar();
		ab.setDisplayShowTitleEnabled(false);
		ab.setDisplayShowHomeEnabled(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_actionbar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add:
			// Pick a contact
			Intent intent = new Intent(Intent.ACTION_PICK);
			intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
			startActivityForResult(intent, PICK_CONTACT);
			break;

		default:
			break;
		}

		return true;
	}

	/**
	 * Handles the contact picker intent result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case PICK_CONTACT:
				// see: http://stackoverflow.com/questions/6155612/getting-number-from-contacts-picker
				Cursor cursor = null;
				String phoneNumber = "";
				List<String> allNumbers = new ArrayList<String>();
				int phoneIdx = 0;
				try {
					// retrieve selected contact data
					Uri result = data.getData();
					String id = result.getLastPathSegment();
					cursor = getContentResolver().query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + "=?", new String[] { id }, null);
					phoneIdx = cursor.getColumnIndex(Phone.DATA);
					if (cursor.moveToFirst()) {
						// data found
						while (cursor.isAfterLast() == false) {
							// add all given phone numbers to a list
							phoneNumber = cursor.getString(phoneIdx);
							allNumbers.add(phoneNumber);
							cursor.moveToNext();
						}
					} else {
						// no data found
						// no results actions
					}

				} catch (Exception e) {
					//error actions
				} finally {
					if (cursor != null) {
						cursor.close();
					}
					// create number selector dialog
					final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
					AlertDialog.Builder builder = new AlertDialog.Builder(ContactsActivity.this);
					builder.setTitle(R.string.choose_a_number);
					builder.setItems(items, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {
							Log.d(TAG, "Numbers: " + items.length);
							addListItemFromNumber(items[item].toString());
						}
					});
					AlertDialog alert = builder.create();
					if (allNumbers.size() > 1) {
						// let user choose which number to use
						alert.show();
					} else {
						if (phoneNumber.length() == 0) {
							//no numbers found actions  
							Toast.makeText(this, R.string.please_select_a_contact_with_a_phone_number, Toast.LENGTH_LONG).show();

						} else {
							Log.d(TAG, "Number: " + phoneNumber);
							// only one number, add contact directly
							addListItemFromNumber(phoneNumber);
						}
					}

				}
				break;
			default:
				Log.d(TAG, "Wrong Intent Reply");
			}
		}

	}

	/**
	 * Creates a contact for the given number and adds it to the list view
	 * 
	 * @param phoneNumber
	 *            The phone number
	 */
	private void addListItemFromNumber(String phoneNumber) {
		String selectedNumber = phoneNumber.toString();
		selectedNumber = selectedNumber.replace("-", "");
		contactList.add(createContactListItemFromNumber(selectedNumber));
		contactArrayAdapter.notifyDataSetChanged();
	};

	@Override
	protected void onDestroy() {
		// store numbers in saved settings
		String allNumbers = "";
		for (int i = 0; i < contactList.size(); i++) {
			allNumbers += contactList.get(i).number + ";";
		}
		GlobalSettings.setContacts(this, allNumbers);
		super.onDestroy();
	}

	/**
	 * Creates a contact item for each phone number from saved settings
	 * 
	 * @param contactList
	 *            The list to add the contacts to
	 */
	private void fillListWithContacts(ArrayList<ContactListItem> contactList) {
		for (String number : phoneNumbers) {
			Log.d(TAG, "number " + number);
			ContactListItem contact = createContactListItemFromNumber(number);
			contactList.add(contact);
		}
	}

	/**
	 * Return a contact with the given number. Contact includes name and image
	 * if present.
	 * 
	 * @param number
	 *            The phone number
	 * @return
	 */
	private ContactListItem createContactListItemFromNumber(String number) {
		String contactId;
		String contactName;
		String[] result;
		result = GlobalSettings.getContact(this, number);

		contactName = result[0];
		contactId = result[1];
		if (contactName.equals("")) {
			contactName = getString(R.string.unknown_name);
		}

		ContactListItem contact = new ContactListItem(contactName, number);
		// Get photo of contactId as input stream:
		if (Integer.parseInt(contactId) != -1) {
			Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId));
			InputStream is = ContactsContract.Contacts.openContactPhotoInputStream(this.getContentResolver(), uri);
			contact.setPhoto(getDrawableImage(is));
		}
		return contact;
	}

	private Drawable getDrawableImage(InputStream image) {
		Drawable d = Drawable.createFromStream(image, "src");
		return d;
	}

}
