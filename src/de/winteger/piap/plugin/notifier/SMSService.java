package de.winteger.piap.plugin.notifier;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;
import de.winteger.piap.plugin.notifier.R;

/**
 * This Service sends a SMS with the given msg and numbers given in the intent
 * 
 * @author sarah
 * 
 */
public class SMSService extends Service {

	private final String TAG = "LoggerNotification";
	
	private int uniqueId = 0;


	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		uniqueId = startId;
		Log.d(TAG, "about to send a SMS");

		Bundle b = intent.getBundleExtra("data");

		String message = "";
		String phoneNumber = "";

		// if data is present
		if (b != null) {
			// read out msg and number
			message = b.getString("message");
			if (message == null || message.equals("")){
				message = getString(R.string.notification_sorry_no_message_given);
			}
			phoneNumber = b.getString("number");
			Log.d(TAG, "bundle data: " + phoneNumber + " " + message);
		}
		// send SMS
		sendSMS(phoneNumber, message);

		// this service is not restarted automatically if it is terminated
		return Service.START_NOT_STICKY;
	}

	/**
	 * This method sends a SMS to the given number with the given text
	 * 
	 * @param phoneNumber
	 *            The telephone number
	 * @param message
	 *            The text
	 */
	private void sendSMS(String phoneNumber, String message) {

		// use intents that are broadcasted, listen for the intents with our receiver..
		PendingIntent piSent = PendingIntent.getBroadcast(getApplicationContext(), uniqueId,
				new Intent(NotificationServiceStarter.SENT).putExtra("number", phoneNumber).putExtra("message", message), 0);
		PendingIntent piDelivered = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(NotificationServiceStarter.DELIVERED), 0);

		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, piSent, piDelivered);
	}

}
